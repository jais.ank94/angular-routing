import { Component, OnInit} from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user: {id: number, name: string};

  constructor( private userService: UserService, private route: ActivatedRoute) { }

  ngOnInit() {
    const id= +this.route.snapshot.params['id'];
    this.user= this.userService.getUser(id);
    this.route.params.subscribe(
      (params: Params) => {
        this.user= this.userService.getUser(+params['id']);
      }
    );
  }
}
